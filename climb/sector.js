module.exports = class EAICSector {
    constructor(mysql) {
        this.mysql = mysql
    }

    process(request, response) {
        const params = request.params;
        const mysql = this.mysql;

        if (params.sector == null || params.sector < 1)
            response.status(400).send({error: "no_sector_set"});
        else {
            const sectorSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_sectors` WHERE `id`='{0}';"
                .format(`${params.sector}`);
            const pathsSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_paths` WHERE `sector_id`='{0}';"
                .format(`${params.sector}`);

            mysql.query(sectorSql, function (error, sectorResult) {
                if (error)
                    response.status(500).send({error: error});
                else
                    mysql.query(pathsSql, function (error, pathsResult) {
                        if (error)
                            response.status(500).send({error: error});
                        else {
                            let zone = sectorResult[0];
                            zone["paths"] = pathsResult;
                            response.status(200).send(zone)
                        }
                    })
            })
        }
    }
}