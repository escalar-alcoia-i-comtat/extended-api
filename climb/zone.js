module.exports = class EAICZone {
    constructor(mysql) {
        this.mysql = mysql
    }

    process(request, response) {
        const params = request.params;
        const mysql = this.mysql;

        if (params.zone == null || params.zone < 1)
            response.status(400).send({error: "no_zone_set"});
        else {
            const zoneSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_zones` WHERE `id`='{0}';"
                .format(`${params.zone}`);
            const sectorsSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_sectors` WHERE `climbing_zone`='{0}';"
                .format(`${params.zone}`);

            mysql.query(zoneSql, function (error, zoneResult) {
                if (error)
                    response.status(500).send({error: error});
                else
                    mysql.query(sectorsSql, function (error, sectorsResult) {
                        if (error)
                            response.status(500).send({error: error});
                        else {
                            let zone = zoneResult[0];
                            zone["sectors"] = sectorsResult;
                            response.status(200).send(zone)
                        }
                    })
            })
        }
    }
}