module.exports =
    class EAICPath {
        constructor(mysql) {
            this.mysql = mysql
        }

        process(request, response) {
            const params = request.params;
            const mysql = this.mysql;

            if (params.path == null || params.path < 1)
                response.status(400).send({error: "no_path_set"});
            else {
                const pathSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_paths` WHERE `id`='{0}';"
                    .format(`${params.path}`);

                mysql.query(pathSql, function (error, pathResult) {
                    if (error)
                        response.status(500).send({error: error});
                    else
                        response.status(200).send(pathResult[0]);
                })
            }
        }
    }
