module.exports = class EAICArea {
    constructor(mysql) {
        this.mysql = mysql
    }

    process(request, response) {
        const params = request.params;
        const mysql = this.mysql;

        if (params.area == null || params.area < 1) {
            const sql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_areas`;";
            mysql.query(sql, function (error, result) {
                if (error)
                    response.status(500).send({error: error});
                else {
                    let builder = [];
                    const areas = result.length;
                    let counter = 0;
                    for (const a in result)
                        if (result.hasOwnProperty(a)) {
                            const area = result[a]
                            const zonesSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_zones` WHERE `area_id`='{0}';"
                                .format(`${area.id}`);
                            mysql.query(zonesSql, function (error, zonesResult) {
                                if (error)
                                    response.status(500).send({error: error});
                                else {
                                    area["zones"] = JSON.parse(JSON.stringify(zonesResult))
                                    builder.push(area)

                                    counter++
                                    if(counter >= areas)
                                        response.status(200).send(builder)
                                }
                            })
                        }
                }
            })
        } else {
            const areaSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_areas` WHERE `id`='{0}';"
                .format(`${params.area}`);
            const zonesSql = "SELECT * FROM `EscalarAlcoiaIComtat`.`climbing_zones` WHERE `area_id`='{0}';"
                .format(`${params.area}`);

            mysql.query(areaSql, function (error, areaResult) {
                if (error)
                    response.status(500).send({error: error});
                else mysql.query(zonesSql, function (error, zonesResult) {
                    if (error)
                        response.status(500).send({error: error});
                    else {
                        let zone = areaResult[0];
                        zone["zones"] = zonesResult;
                        response.status(200).send(zone)
                    }
                })
            })
        }
    }
}