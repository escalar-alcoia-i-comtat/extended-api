module.exports = class FriendRequests {
    constructor(mysql) {
        this.mysql = mysql
    }

    process(request, response) {
        const params = request.params;

        const sql = "SELECT `timestamp`, `uuid`, `from_user` FROM `EscalarAlcoiaIComtat`.`friend_requests` WHERE `consumed`='0' AND `to_user`='{0}';".format(params.user);

        this.mysql.query(sql, function (error, result) {
            if (error)
                response.status(500).send({error: error});
            else {
                response.status(200).send({result: "ok", data: result})
            }
        })
    }
}