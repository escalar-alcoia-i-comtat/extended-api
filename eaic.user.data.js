module.exports = class UserData {
    constructor(mysql) {
        this.mysql = mysql
    }

    process(request, response) {
        const params = request.params;

        const sql = "SELECT * FROM `EscalarAlcoiaIComtat`.`users` WHERE `uid`='{0}';"
            .format(params.user);

        this.mysql.query(sql, function (error, result) {
            if (error)
                response.status(500).send({error: error});
            else
                response.status(200).send({result: "ok", data: result})
        })
    }
}